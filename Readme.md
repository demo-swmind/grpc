# Wstęp do GRPC

### HTTP/2 + PROTO* = GRPC
PROTO* (by default)
### GRPC =/= REST

https://grpc.io/docs/what-is-grpc/core-concepts/ \
https://yidongnan.github.io/grpc-spring-boot-starter/en/server/getting-started.html \
https://www.udemy.com/course/grpc-the-complete-guide-for-java-developers/

## HTTP/2
***HTTP/2*** makes our applications faster, simpler, and more robust. Reducing latency by enabling request and response multiplexing, adding efficient compression of HTTP header fields, and adding support for request prioritization and server push.

Less number of connections = less expensive TLS handshakes, more efficient session reuse, reducing client and server resources.

### HTTP/1.1 vs HTTP/2

***HTTP/1.1 has***

 - Textual format
 - Plain Text Headers
 - TCP connection requires 3 way hand shake process (single request and response with 1 single TCP connection)

***HTTP/2 brings***

 - Binary format
 - Header Compression
 - Flow Control
 - Multiplexing (Same TCP connection can be reused for multiplexing. Server streaming — Client streaming — Bi-directional streaming is possible)

## PROTO
***Protocol Buffers***, a powerful binary serialization toolset and language, and provides tools for generating clients and servers across different languages.

    syntax = "proto3";

    message HelloRequest {
        string firstName = 1;
        string lastName = 2;
    }
    
    message HelloResponse {
        string greeting = 1;
    }
    
    service HelloService {
        rpc hello(HelloRequest) returns (HelloResponse);
    }

### Typy Danych

- int32 (for int) — default value: 0
- int64 (for long) — default value: 0
- float — default value: 0
- double — default value: 0
- bool — default value: false
- string — default value: empty string
- bytes (for byte[])
- repeated (for List/Collection)
- map (for Map) — default value: empty map
- enum — default value: first value in the list of values
- **i więcej** (importing wrappers)

# DEMO

Trzy moduły:
- **INTERFACE** (sharowany) - definicje proto plus wygenerowane java klasy (model i service)
- **SERVER** - serwer implementujacy "INTERFACE"
- **CLIENT** - jeden/wiele projektów wykorzystujacych wygenerowane stuby

![](static/client-project-setup.svg)

### DEMO (interface + unary server + grpc curl)

https://github.com/fullstorydev/grpcurl

    grpcurl --plaintext localhost:9090 list
    grpcurl --plaintext localhost:9090 list com.example.grpc.lib.PingPongService
    grpcurl --plaintext -d "{\"ping\":\"test\"}" localhost:9090 com.example.grpc.lib.PingPongService/PingPong

# CECHY - Streaming

- Unary\
`rpc SayHello(HelloRequest) returns (HelloResponse);`
- Server streaming\
`rpc LotsOfReplies(HelloRequest) returns (stream HelloResponse);`
- Client streaming\
`rpc LotsOfGreetings(stream HelloRequest) returns (HelloResponse);`
- Bidirectional streaming\
`rpc BidiHello(stream HelloRequest) returns (stream HelloResponse);`

https://grpc.io/docs/what-is-grpc/core-concepts/
### DEMO (client + streaming)

# CECHY - Wydajność GRPC vs REST 😀

![](static/grpc-vs-rest-response-time.png)

https://www.vinsguru.com/grpc-vs-rest-performance-comparison/

# CECHY - Implementacje 😐

SpringBootStarter

yidongnan | LogNet
--- | ---
![alt text](static/yidongnan.PNG) | ![alt text](static/LogNet.PNG)
https://github.com/yidongnan/grpc-spring-boot-starter | https://github.com/LogNet/grpc-spring-boot-starter

# CECHY - Loadbalancing 😐

![](static/grpc-lb.PNG)
## Recommendations and best practices
![](static/lb-table.PNG)
https://kubernetes.io/blog/2018/11/07/grpc-load-balancing-on-kubernetes-without-tears/

# Inne
## High-level Components
At a high level there are three distinct layers to the library: Stub, Channel, and Transport.
Stub

- **The Stub layer** is what is exposed to most developers and provides type-safe bindings to whatever datamodel/IDL/interface you are adapting. gRPC comes with a plugin to the protocol-buffers compiler that generates Stub interfaces out of .proto files, but bindings to other datamodel/IDL are easy and encouraged.
Channel

- **The Channel layer** is an abstraction over Transport handling that is suitable for interception/decoration and exposes more behavior to the application than the Stub layer. It is intended to be easy for application frameworks to use this layer to address cross-cutting concerns such as logging, monitoring, auth, etc.
Transport

- **The Transport layer** does the heavy lifting of putting and taking bytes off the wire. The interfaces to it are abstract just enough to allow plugging in of different implementations. Note the transport layer API is considered internal to gRPC and has weaker API guarantees than the core API under package io.grpc.

gRPC comes with three Transport implementations:
- The Netty-based transport is the main transport implementation based on Netty. It is for both the client and the server.
- The OkHttp-based transport is a lightweight transport based on OkHttp. It is mainly for use on Android and is for client only.
- The in-process transport is for when a server is in the same process as the client. It is useful for testing, while also being safe for production use.

## Obsługa błędów

Przykład

```
@GrpcAdvice
public class GrpcExceptionAdvice {


    @GrpcExceptionHandler
    public Status handleInvalidArgument(IllegalArgumentException e) {
        return Status.INVALID_ARGUMENT.withDescription("Your description").withCause(e);
    }

    @GrpcExceptionHandler(ResourceNotFoundException.class)
    public StatusException handleResourceNotFoundException(ResourceNotFoundException e) {
        Status status = Status.NOT_FOUND.withDescription("Your description").withCause(e);
        Metadata metadata = ...
        return status.asException(metadata);
    }

}
```

## Warte wspomnienia

- Deadlines/Timeouts
- Security
- Testowanie

# Dodatkowe linki
https://www.udemy.com/course/grpc-the-complete-guide-for-java-developers/
https://yidongnan.github.io/grpc-spring-boot-starter
