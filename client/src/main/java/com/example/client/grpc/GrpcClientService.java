package com.example.client.grpc;

import com.example.grpc.lib.PingPongServiceGrpc;
import com.example.grpc.lib.PingRequest;
import com.example.grpc.lib.PongResponse;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executor;
import java.util.stream.IntStream;

@Slf4j
@RequiredArgsConstructor
@Service
public class GrpcClientService {

    private static final String PING = "SOME-TEST";

    @GrpcClient("some-grpc-server")
    private PingPongServiceGrpc.PingPongServiceBlockingStub blockingStub;

    @GrpcClient("some-grpc-server")
    private PingPongServiceGrpc.PingPongServiceStub asyncStub;

    @GrpcClient("some-grpc-server")
    private PingPongServiceGrpc.PingPongServiceFutureStub futureStub;

    private final Executor executor;

    @PostConstruct
    public void init() {

        testBlocking();
        testFuture();
        testStream();
        // BONUS
        //testStreamStream();

        log.info("...GRPC TEST END");
    }

    private void testBlocking() {
        // BLOCKING
        log.info("GRPC request BLOCKING...");
        try {
            log.info(" RESPONSE: " + this.blockingStub.pingPong(createPingRequest(PING)).getPong());
        } catch (final StatusRuntimeException e) {
            log.info(" FAILED with: " + e.getStatus().getCode().name());
        }
    }

    private void testFuture() {
        // FUTURE LISTENER
        log.info("GRPC request FUTURE...");
        ListenableFuture<PongResponse> listenableFuture = futureStub.pingPong(createPingRequest(PING));
        Futures.addCallback(listenableFuture, new FutureCallback<>() {
            @Override
            public void onSuccess(PongResponse result) {
                log.info(" RESPONSE ASYNC: " + result.getPong());
            }

            @Override
            public void onFailure(Throwable throwable) {
                log.error(" RESPONSE FAILURE: " + throwable.getMessage());
            }
        }, executor);
    }

    private void testStream() {
        // STREAM OBSERVER
        log.info("GRPC request STREAM observer...");
        asyncStub.pingPong(createPingRequest(PING), new StreamObserver<>() {
            @Override
            public void onNext(PongResponse result) {
                log.info(" RESPONSE STREAM NEXT: " + result.getPong());
            }

            @Override
            public void onError(Throwable throwable) {
                log.info(" RESPONSE STREAM ERROR: " + throwable.getMessage());
            }

            @Override
            public void onCompleted() {
                log.info(" RESPONSE STREAM CPLT");
            }
        });
    }

    // BONUS (diff method)
    private void testStreamStream() {
        // STREAM-STREAM OBSERVER
        log.info("GRPC request STREAM-STREAM observer...");
        StreamObserver<PingRequest> requestStreamObserver = asyncStub.pingPongStream(new StreamObserver<>() {
            @Override
            public void onNext(PongResponse result) {
                log.info(" RESPONSE STREAM-STREAM NEXT: " + result.getPong());
            }

            @Override
            public void onError(Throwable throwable) {
                log.info(" RESPONSE STREAM-STREAM ERROR: " + throwable.getMessage());
            }

            @Override
            public void onCompleted() {
                log.info(" RESPONSE STREAM-STREAM CPLT");
            }
        });
        IntStream.rangeClosed(1, 10).forEach(value -> {
            requestStreamObserver.onNext(createPingRequest("PING_IDX_" + value));
        });
        requestStreamObserver.onCompleted();
    }

    private static PingRequest createPingRequest(String name) {
        return PingRequest.newBuilder().setPing(name).build();
    }

}
