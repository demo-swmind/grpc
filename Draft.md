# LIB
https://yidongnan.github.io/grpc-spring-boot-starter/en/client/getting-started.html
https://github.com/yidongnan/grpc-spring-boot-starter
https://github.com/yidongnan/grpc-spring-boot-starter/blob/master/examples/local-grpc-client
https://github.com/LogNet/grpc-spring-boot-starter
https://sajeerzeji44.medium.com/grpc-for-spring-boot-microservices-bd9b79569772


https://www.udemy.com/course/grpc-the-complete-guide-for-java-developers/
https://www.vinsguru.com/grpc-vs-rest-performance-comparison/
https://medium.com/turkcell/grpc-implementation-with-spring-boot-7d6f98349d27
https://www.baeldung.com/grpc-introduction

# HEADER
https://github.com/grpc/grpc-java/tree/master/examples/src/main/java/io/grpc/examples

#CORE
https://github.com/grpc/grpc-java
https://grpc.io/docs/what-is-grpc/core-concepts/

#OTHER
https://spring.io/blog/2021/12/08/spring-cloud-gateway-and-grpc

#LB
https://www.vinsguru.com/grpc-load-balancing-with-nginx/
https://www.vinsguru.com/grpc-on-kubernetes-with-linkerd/
