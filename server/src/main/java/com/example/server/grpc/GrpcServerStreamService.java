package com.example.server.grpc;

import com.example.grpc.lib.PingPongServiceGrpc;
import com.example.grpc.lib.PingRequest;
import com.example.grpc.lib.PongResponse;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

import static com.example.server.grpc.GrpcServerService.doHardcoreProcessing;

//@GrpcService
public class GrpcServerStreamService extends PingPongServiceGrpc.PingPongServiceImplBase {

    @Override
    public StreamObserver<PingRequest> pingPongStream(StreamObserver<PongResponse> responseObserver) {
        return new StreamObserver<>() {
            @Override
            public void onNext(PingRequest input) {
                responseObserver.onNext(doHardcoreProcessing(input));
            }

            @Override
            public void onError(Throwable throwable) {
                responseObserver.onCompleted();
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }

}
