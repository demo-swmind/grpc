package com.example.server.grpc;

import com.example.grpc.lib.PingPongServiceGrpc;
import com.example.grpc.lib.PingRequest;
import com.example.grpc.lib.PongResponse;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class GrpcServerService extends PingPongServiceGrpc.PingPongServiceImplBase {

    @Override
    public void pingPong(PingRequest input, StreamObserver<PongResponse> responseObserver) {
        responseObserver.onNext(doHardcoreProcessing(input));
        responseObserver.onCompleted();
    }

    public static PongResponse doHardcoreProcessing(PingRequest request) {
        return PongResponse.newBuilder().setPong("PONG to " + request.getPing()).build();
    }
}
